import express from "express";
//Instalar modulo morgan para ver las peticiones que llegan por consola
//              npm i morgan
import morgan from "morgan";
//Instalar modulo cors para tener el mecanismo de recursos de origen cruzados.
//              npm i morgan
import cors from "cors";
import TasksRoutes from "./routes/tasks.routes";

const app = express();

//settings
app.set("port", process.env.PORT || 3000);

//middlewares
const corsOptions = {}; //Lista de objetos con los servidores que pueden resivir
app.use(cors(corsOptions));
app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// routes
app.get("/", (req, res) => {
  res.json({ message: "Welcome to my application" });
});

app.use("/api/tasks", TasksRoutes);

export default app;
