//Para usar MongoDB en NODE utilizar el modulo mongoose
//                       npm i mongoose
//Para utilizar una variable de entorno para la base de datos instalar el modulo dotenv:                  npm i dotenv
import mongoose from "mongoose";
import config from "./config";

(async () => {
  try {
    const db = await mongoose.connect(config.mongodbURL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    });
    console.log("Database is connected to:", db.connection.name);
  } catch (error) {
    console.error(error);
  }
})();
