//Iniciar el Proyecto NODE:  npm init -y
//Modulo de Express:        npm i express
//Modulo de Babel para trabajar con codigo moderno de JavaScript, poder ejecutar en consola, que se integre a Node.js y por ultimo el modulo para tener la sintaxis de JS:
//                       npm i @babel/core @babel/cli @babel/node @babel/preset-env -D
//Para ejecutar codigo babel en produccion npm i -D @babel/plugin-transform-runtime
//El -D hace que las dependecias se agregen a parte, para que solo se usen cuando se este desarrollando.
//Instalar el modulo nodemon para que el server se auto ejecute con cada modificacion:
//                       npm i nodemon -D
import app from "./app";
import "./database";

app.listen(app.get("port"));

console.log("Server on port", app.get("port"));
